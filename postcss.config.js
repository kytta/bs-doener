const autoprefixer = require("autoprefixer");
const csso = require("postcss-csso");
const cssVariables = require("postcss-css-variables");
const purgecss = require("@fullhuman/postcss-purgecss");

module.exports = {
	plugins: [
		purgecss({
			content: ["./public/**/*.html"],
		}),
		cssVariables({ preserve: false }),
		autoprefixer(),
		csso(),
	],
};
