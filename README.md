# Die Braunschweiger Döner-Studie

> Die Suche nach dem besten Dönerladen in Braunschweig!

## Lizenz

_Die Braunschweiger Döner-Studie_ von [Nikita Karamov](https://karamoff.dev/)
ist mit [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
gekennzeichnet und damit der Öffentlichkeit gewidmet.
